DROP DATABASE IF EXISTS `nodechat`;
CREATE DATABASE IF NOT EXISTS `nodechat` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `nodechat`;

CREATE TABLE IF NOT EXISTS `accounts` (
  `id_user` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL UNIQUE,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `avatar` varchar(255),
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `accounts` (`id_user`, `username`, `password`, `email`, `avatar`) VALUES (1, 'test', 'test', 'test@test.com', 'https://focus.telerama.fr/2022/12/06/0/165/3191/1800/1200/0/60/0/4df0f50_1670335245220-wednesday-s1-e4-00-34-26-18r.jpg');
INSERT INTO `accounts` (`id_user`, `username`, `password`, `email`, `avatar`) VALUES (2, 'lala', 'lala', 'lala@lala.com', 'https://images4.alphacoders.com/668/668521.jpg');


CREATE TABLE IF NOT EXISTS `groupchat` (
  `idgroup` int(15) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(50),
  `description` varchar(150),
  PRIMARY KEY (`idgroup`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `groupchat` (`idgroup`, `groupname`, `description`) VALUES (1, 'mangeur', 'description du groupe mangeur');
INSERT INTO `groupchat` (`idgroup`, `groupname`, `description`) VALUES (2, 'tueur', 'description du groupe de Mercredi');

CREATE TABLE IF NOT EXISTS `belonggroup` (
  `id_user` INT NOT NULL,
  `id_group` INT NOT NULL,
  FOREIGN KEY (`id_user`) REFERENCES `accounts`(`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`id_group`) REFERENCES `groupchat`(`idgroup`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `belonggroup` (`id_user`, `id_group`) VALUES (1, 1);
INSERT INTO `belonggroup` (`id_user`, `id_group`) VALUES (2, 1);
INSERT INTO `belonggroup` (`id_user`, `id_group`) VALUES (1, 2);

CREATE TABLE IF NOT EXISTS `message`(
    `id_user` INT NOT NULL,
    `id_group` INT NOT NULL,
    `id_mess` INT NOT NULL AUTO_INCREMENT,
    `message` varchar(499) NOT NULL,
    `date` DATETIME NOT NULL,
    FOREIGN KEY (`id_user`) REFERENCES `accounts`(`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`id_group`) REFERENCES `groupchat`(`idgroup`) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (`id_mess`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



INSERT INTO `message` (`id_user`, `id_group`,`id_mess`, `message`, `date`) VALUES (1, 1, 1, 'message de test',NOW());
INSERT INTO `message` (`id_user`, `id_group`,`id_mess`, `message`, `date`) VALUES (2, 1, 2, 'Souhaiter la bienvenue au nouveau arrivant', NOW());

/* DAUTRE INSERT */

INSERT INTO `message` (`id_user`, `id_group`,`id_mess`, `message`, `date`) VALUES (1, 2, 3, 'message de test avec Mercredi', NOW());
