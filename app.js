
var express = require('express');
var app = express();
const server = require('http').createServer(app);
const mariadb = require('mariadb');
let session = require('cookie-session'); // Charge le middleware de sessions
let bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
let urlencodedParser = bodyParser.urlencoded({ extended: false });
const path = require('path');
//const fs = require('fs');

const io = require('socket.io')(server);
const nun = require('nunjucks');
const { fstat } = require('fs');

userModule = require('./controller/userModule.js');
groupModule = require('./controller/groupModule.js');


    console.log(process.env.MYSQLDB_USER);
    console.log(process.env.MYSQLDB_ROOT_PASSWORD);
    console.log(process.env.MYSQLDB_DATABASE);
    console.log(process.env.DB_PORT);
    
const pool = mariadb.createPool({
    host: "mariadbcomp",
    user: process.env.MYSQLDB_USER,
    password: process.env.MYSQLDB_ROOT_PASSWORD,
    database: process.env.MYSQLDB_DATABASE,
    port: 3306
});

nun.configure('views', {
    autoescape: true,
    express: app
});

var rooms = ['room1'];

app.use(express.static(__dirname + '/views/css'));


/* On utilise les sessions */
app.use(session({ secret: 'chat' }))
    .use((req, res, next) => {
        // on initialise cette liste si besoin
        if(typeof req.session.loggedin == 'undefined'){
            req.session.loggedin = false;
            req.session.user = undefined;
        }
        next();
    })

    .get('/login', (req, res) => {
        if (req.session.loggedin) {
            res.redirect('/user');
        } else {
            res.render('connect.html');
        }
    })

    .post('/login',  urlencodedParser, function (req, res) {
        let username = req.body.username;
        let password = req.body.password;
        if (username && password ) {
            userModule.userConnection(username, password, pool).then( rows => {
                if (typeof (rows[0]) != 'undefined') {
                    req.session.loggedin = true;
                    req.session.user = rows[0];
                    res.redirect('/user');
                } else {
                    res.render('connect.html', {error: "Erreur pseudo ou mot de passe !"});
                }
            });
        } else {
            res.redirect('/login');
        }
    })

    .get('/register', (req, res) => {
        if (req.session.loggedin) {
            res.redirect('/user');
        } else {
            res.render('register.html');
        }
    })

    .post('/register',  urlencodedParser, function (req, res) {
        let username = req.body.username;
        let email = req.body.email;
        let password = req.body.password;
        let avatar = req.body.avatar;
        if (username && password && email) {
            userModule.userRegister(username, password,email, avatar, pool).then( rows => {
                let patternPseudoExistant = /no: 1062/;
                if (!patternPseudoExistant.test(rows)) {
                    userModule.userConnection(username, password, pool).then( rows => {
                        if (typeof (rows[0]) != 'undefined') {
                            req.session.loggedin = true;
                            req.session.user = rows[0];
                            res.redirect('/user');
                        }})
                } else {
                    res.render('register.html', {error: "Pseudo déjà existant !"});
                }
            });
        } else {
            res.redirect('/register', {erreur: 'Il manque des champs non remplis'});
        }
    })

    .get('/logout', (req, res) => {
        req.session.loggedin = false;
        req.session.user = undefined;
        res.redirect('/login');
    })


    .get('/user', (req, res) => {
        if (req.session.loggedin) {
            userModule.getGroupList(req.session.user.id_user, pool).then( rows => {
                res.render('index.html', {pseudo: req.session.user.username, group_list: rows, avatar: req.session.user.avatar, id_user:req.session.user.id_user});
            })
        } else {
            res.redirect('/login');
        }
    })

    .get('/user/:id', (req, res, currentGrp) => {
        if (req.session.loggedin) {
            userModule.getGroupList(req.session.user.id_user, pool).then( rows => {           
                rows.forEach(function(row){
                    if (row["idgroup"]==req.params.id){
                        res.render('index.html', {pseudo: req.session.user.username, group_list: rows, avatar: req.session.user.avatar, currentGroupe: currentGrp, id_user:req.session.user.id_user, group:row});
                    }
                });                    
            })
        } else {
            res.redirect('/login');
        }
    })
    
    .get('/add/group', (req, res) => {
        if (req.session.loggedin) {
            userModule.getGroupList(req.session.user.id_user, pool).then( rows => {
                res.render('add_group.html', {pseudo: req.session.user.username, group_list: rows, avatar: req.session.user.avatar, id_user:req.session.user.id_user});
            })
        } else {
            res.redirect('/login');
        }
    })
    
    .post('/add/group',  urlencodedParser, function (req, res) {
        let groupname = req.body.groupname;
        let description = req.body.description;
        if (groupname && description) {
            groupModule.addGroup(groupname, description, req.session.user.id_user, pool).then( rows => {
                res.redirect('/user');
            })
        } else {
            res.redirect('/add/group', {erreur: 'Il manque des champs non remplis'});
        }
    })

    .get('/delete/group/:id', (req, res, id_group) => {
        if (req.session.loggedin) {
            res.render('delete_group.html', {id_group: req.params.id});
        } else {
            res.redirect('/login');
        }
    })

    .post('/delete/group/:id',  urlencodedParser, function (req, res, id_group) {  
        if (req.params.id && req.session.user.id_user) {
            groupModule.deleteGroup(req.params.id, req.session.user.id_user, pool).then( rows => {
                res.redirect('/user');
            })
        } else {
            res.redirect('/delete/group/'+req.params.id, {erreur: 'Erreur dans la supression'});
        }
    })
    
    .get('/add/group_user/:id', (req, res) => {
        if (req.session.loggedin) {
            groupModule.getUserNotGroup(req.params.id, pool).then( rows => {
                res.render('add_groupuser.html', {id_group: req.params.id, user_list: rows});
            })
        } else {
            res.redirect('/login');
        }
    })
    
    .post('/add/group_user/:id',  urlencodedParser, function (req, res) {
        let newuser = req.body.user;
        if (newuser) {
            groupModule.addGroupUser(req.params.id, newuser, pool).then( rows => {
                res.redirect('/user/'+req.params.id);
            })
        } else {
            res.redirect('/add/group_user/'+req.params.id, {erreur: 'Il manque des champs non remplis'});
        }
    });
    

        
io.on('connection', (socket) => {
    socket.join('generale');
    socket.on("sendchat", data => { //ca recupere id_group dans l'url donc
        if (!isNaN(parseInt(data.id_group))) { //si c'est pas digit c'est que l'utilisisateur n'est pas dans un groupe
            if (data.msg != ''){
                userModule.addMessage( {id_user: data.id_user, id_group: data.id_group, message: data.msg}, pool).then( rows => { //ajoute le message en BD
                    let id_mess = rows['insertId']
                    if (rows['insertId'][rows['insertId'].length] == 'n'){
                        id_mess = rows['insertId'][0,rows['insertId'].length -1 ]
                    }
                    console.log("app.js"+id_mess);
                    io.sockets.in(socket.room).emit('updatechat',{msg: data.msg,data, pseudo: data.pseudo, avatar: data.avatar, id_messa:id_mess.toString()}); //envoie le message aux clients 
                })
            }
            
        }else {
            if (data.msg != '') io.sockets.in(socket.room).emit('updatechat',{msg: data.msg,data, pseudo: data.pseudo, avatar: data.avatar, id_messa: 0}); //envoie le message aux clients sans mettre dans BD
            
        } 
        if (data.file_data != ""){
            io.sockets.in(socket.room).emit('updatechat_image',{msg: data.file_data, pseudo: data.pseudo, avatar: data.avatar}); //envoie le message aux clients 
        }

    });
    socket.on('delete_message', (idMsg) => {
        console.log("app.js :"+ idMsg);
        groupModule.deleteMessage(idMsg, pool).then( rows => {
            io.emit('delete_message', idMsg);
        })
        
    })
    socket.on('adduser', function(username, room){
		// we store the username in the socket session for this client
		socket.username = username;
        socket.room = room;
        socket.join(room);
	});


    socket.on('switchRoom', function(newroom){
		// leave the current room (stored in session)
		socket.leave(socket.room);
		// join new room, received as function parameter
		socket.join(newroom);
		// update socket session room title
        socket.room = newroom;
        
        userModule.getMessagesFromGroup(newroom, pool).then( rows => {
            socket.emit('updaterooms', rooms, newroom, rows);
        })  
	});



    // when the user disconnects.. perform this
	socket.on('disconnect', function(){
		// update things
	});

});

server.listen(3002, () => console.log('the app is running on localhost:3002/user'));