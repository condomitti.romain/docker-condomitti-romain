async function userConnection(username, password, pool) {
  let conn;
  try {
    conn = await pool.getConnection();
    let res =  await conn.query('SELECT * FROM nodechat.accounts WHERE username = ? AND password = ?', [username, password]);
    conn.end();
    return res;
  } catch (err) {
    throw err;
  }
}

async function userRegister(username, password, email, avatar, pool) {
  let conn;
  try {
    conn = await pool.getConnection();
    let res = await conn.query('INSERT INTO nodechat.accounts(username, password, email, avatar) values (?,?,?,?)', [username, password, email, avatar]);
    conn.end();
    return res;
  } catch (err) {
    return err;
  }
}

async function getGroupList(user_id, pool) {
  let conn;
  try {
    conn = await pool.getConnection();
    let res = await conn.query('SELECT * FROM nodechat.groupchat WHERE idgroup IN ( SELECT id_group FROM nodechat.belonggroup WHERE nodechat.belonggroup.id_user = ?)', [user_id]);
    conn.end();
    return res;
  } catch (err) {
    return err;
  }
}

async function getMessagesFromGroup(group_id, pool) {
  let conn;
  try {
    conn = await pool.getConnection();
    let res = await conn.query('SELECT * FROM nodechat.message natural join nodechat.accounts WHERE id_group = ? order by date', [group_id]);
    let res2 = await conn.query('SELECT * FROM nodechat.accounts natural join nodechat.belonggroup WHERE id_group = ? order by username', [group_id]);
    conn.end();
    return [res, res2];
  } catch (err) {
    return err;
  }
}

async function addMessage(data, pool) {
  let conn;
  try {
    conn = await pool.getConnection();
    console.log(data)
    let res = await conn.query('INSERT INTO nodechat.message (`id_user`, `id_group`,`message`, `date`) VALUES (?, ?, ?, NOW())', [data.id_user, data.id_group, data.message]);
    conn.end();
    return res;
  } catch (err) {
    console.log(err);
    return err;
  }
}



exports.userConnection = userConnection;
exports.userRegister = userRegister;
exports.getGroupList = getGroupList;
exports.getMessagesFromGroup = getMessagesFromGroup;
exports.addMessage = addMessage;