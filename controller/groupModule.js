async function addGroup(groupname, description, id_user, pool) {
    let conn;
    try {
      conn = await pool.getConnection();
      let add = await conn.query('INSERT INTO nodechat.groupchat(groupname, description) values (?,?)', [groupname, description]);
      let id_group = await conn.query('SELECT MAX(idgroup) as id FROM nodechat.groupchat');
      let res = await conn.query('INSERT INTO nodechat.belonggroup(id_user, id_group) values (?,?)', [id_user, id_group[0]['id']]);
      conn.end();
      return res;
    } catch (err) {
      return err;
    }
}

async function deleteGroup(id_group, id_user, pool) {
  let conn;
  try {
    conn = await pool.getConnection();
    let res = await conn.query('DELETE FROM nodechat.belonggroup WHERE id_user= ? and id_group= ?', [id_user, id_group]);
    console.log(res);
    conn.end();
    return res;
  } catch (err) {
    return err;
  }
}

async function getUserNotGroup(id_group, pool) {
  let conn;
  try {
    conn = await pool.getConnection();
    let res =  await conn.query('SELECT * FROM nodechat.accounts WHERE id_user NOT IN (SELECT id_user FROM nodechat.accounts natural join nodechat.belonggroup WHERE id_group = ?) order by username', [id_group]);    
    conn.end();
    return res;
  } catch (err) {
    return err;
  }
}
  
async function deleteMessage(id_msg, pool) {
  let conn;
  try {
    conn = await pool.getConnection();
    let res = await conn.query('DELETE FROM nodechat.message WHERE id_mess= ?', [id_msg]);
    conn.end();
    return res;
  } catch (err) {
    return err;
  }
}

async function addGroupUser(id_group, user, pool) {
  let conn;
  try {
    conn = await pool.getConnection();
    let res = await conn.query('INSERT INTO nodechat.belonggroup(id_user, id_group) values (?,?)', [user, id_group]);
    let add = await conn.query('INSERT INTO nodechat.message(id_user, id_group, message, date) values (?,?,?, NOW())', [user, id_group, 'Souhaiter la bienvenue au nouvelle arrivant']);
    conn.end();
    return res;
  } catch (err) {
    return err;
  }
}



exports.addGroup = addGroup;
exports.deleteMessage = deleteMessage;
exports.deleteGroup = deleteGroup;
exports.getUserNotGroup = getUserNotGroup;
exports.addGroupUser = addGroupUser;
