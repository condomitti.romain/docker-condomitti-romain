FROM node:latest

WORKDIR .
COPY . .
RUN npm install
VOLUME [ "/node_modules" ]
CMD [ "node","app.js" ]

#end of Dockerfile